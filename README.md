# docker-in-docker using a self hosted runner and the docker image daemon

Examples of docker in docker usage with a self hosted runner (on cloudstack for example). In this example we will use the docker daemon of the docker image.

## General configuration

To run this project you first have to:

1. [Enable project CI/CD feature](https://gitlab.inria.fr/gitlabci_gallery#enable-shared-runners)
2. [Enable container registry](https://gitlab.inria.fr/gitlabci_gallery#enable-container-registry).

## Register the project runner

Then you have to define a new project runner in GitLab:

- On the left sidebar in the Gitlab interface, go to **Settings** → **CI/CD**. If you don't have a **CI/CD** item, then you should [enable project CI/CD feature](https://gitlab.inria.fr/gitlabci_gallery#enable-shared-runners)  first.
- Expand **Runners**.
- Click on **New project runner** button.
- Choose **Linux** as OS and set the tags you want for your runner. We set **container daemon, docker, linux** for this project. 
- Click **Create runner** and copy the token in a safe place.

Connect to your VM and:
- Install gitlab runner. If you use a Cloudstack VM you can follow the instructions given in the [CI documentation](https://inria-ci.gitlabpages.inria.fr/doc/page/gitlab/#installation-example-on-a-gnulinux-slave-from-ciinriafr-ci-user-account). Else you can refer to the [GitLab documentation(https://docs.gitlab.com/runner/install/)].
- [Install docker](https://docs.docker.com/get-docker/).
- Register the runner as sudo with the following command:
```bash
sudo gitlab-runner register --url https://gitlab.inria.fr --token YOUR-TOKEN
```
    - choose **docker** as executor
    - choose **docker:24.0.5** as default Docker image
- Check your runner have beeen successfully registered with `gitlab-runner run` command. Your runner must appear with a green point on  **Settings** → **CI/CD** → **Runners**.

### Setting your runner to launch a docker daemon in the container:

To do so we have to modify the runner configuration file to set privileged mode to the container.
Connect your VM as sudo and open the config file: `sudo vi /etc/gitlab-runner/config.toml` file. Then set `privileged` to `true`:

```toml
name = "docker-in-docker-container-daemon"
  url = "https://gitlab.inria.fr"
  id = 7294
  token = XXXX
  token_obtained_at = 2024-03-14T16:46:27Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "docker:24.0.5"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0
```

Restart the gitlab-runner service (`suso gitlab-runner restart`).

In the *.gitlab-ci.yml* , you have also to:
    
* Specify where the docker socket is. It is done with the *DOCKER_HOST* variable.
* Specify that docker should not start over tls. It is done with the *DOCKER_TLS_CERTDIR* variable.
* Start a docker daemon in the first stage that is building the image. It is done by the *services* keyword.

